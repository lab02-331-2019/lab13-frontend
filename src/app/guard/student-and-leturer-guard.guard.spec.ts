import { TestBed, async, inject } from '@angular/core/testing';

import { StudentAndLeturerGuardGuard } from './student-and-leturer-guard.guard';

describe('StudentAndLeturerGuardGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StudentAndLeturerGuardGuard]
    });
  });

  it('should ...', inject([StudentAndLeturerGuardGuard], (guard: StudentAndLeturerGuardGuard) => {
    expect(guard).toBeTruthy();
  }));
});
